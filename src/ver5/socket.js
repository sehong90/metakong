// socket.io 서버에 접속한다
var socket = io("wss://metaforming.com:3000");
//var socket = io('http://localhost:5010');

var players = [];
var myPeer = null;
const peers = {};
var conn = null;

$(document).ready(function () {
   
    $('#chat_msg').keypress(function (evt) {
        if((evt.keyCode || evt.which) == 13){
            evt.preventDefault();
            send();
        }
    });
    // 서버로 자신의 정보를 전송한다.
    socket.emit("login", new BABYLON.Vector3(3, 0.07, 35));
});
// 총 접속자 수
socket.on("clientsCount", function(data) {
  $("#clients_count").html("현재 접속자 수 : "+data);
});

// 유저 닉네임 변경
socket.on("userNameUpdate", function(data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      updatePlayerNameLabel(i, data.userName);
      break;
    }
  }
});

// 유저 입장
socket.on("login", function(playerList, newPlayer) {
  $('#name').val(newPlayer.userName);
  player_info.name=newPlayer.userName;
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + newPlayer.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  setNameLabel(newPlayer.socketId);
  myPeer = new Peer(socket.id);

  // 기존 유저 생성
  for(var i=0 ; i<playerList.length; i++){
    createPlayer(playerList[i].player);
  }

  myPeer.on('call', function (call) {
    console.log("콜 받음");
    call.answer(null);
    call.on('stream', function (stream) {
      addVideoStream(stream, call.metadata.streamer);
    });
  });
});

// 다른 유저 입장
socket.on("createPlayer", function(data) {
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + data.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  createPlayer(data);

});

// 유저 나감
socket.on("disconnectPlayer", function(data) {
  var state = true;
  for (var i = 0; i < players.length; i++) {
    for (var j = 0; j< data.length; j++) {
      if (data[j].player.socketId == players[i].socketId) {
        state = false;
        break;
      }
    }
    if(!state){
      $("#chat_content").append("<div>[안내] <strong>" + data[j].player.userName+ "</strong>님 나감!</div>");
      $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
      removeAvatar(players[i]);
      players.splice(i, i);
    }
  }
});

// 유저 이동
socket.on("positionUpdate", function(positionData) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == positionData.socketId) {
      index = i;
      break;
    }
  }
  if(index > -1){
  
  let player = players[index];
  player.player.position = positionData.position;
  player.player.rotation =new BABYLON.Vector3(0,positionData.rotation._y,0);
  player.nameTag.position = new BABYLON.Vector3(positionData.position._x, positionData.position._y + 2, positionData.position._z);
  }
});

// 유저 액션 변경
socket.on("animationUpdate", function(data) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      index = i;
      break;
    }
  }
  if(index > -1){
    let player = players[index].player;

    // 아바타 애니메이션 설정
    player.skeleton.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
    player.skeleton.animationPropertiesOverride.enableBlending = true;
    player.skeleton.animationPropertiesOverride.blendingSpeed = 1;
    player.skeleton.animationPropertiesOverride.loopMode = 1;

    var idleRange = skeleton.getAnimationRange("YBot_Idle");
    var walkRange = skeleton.getAnimationRange("YBot_Walk");
    var runRange = skeleton.getAnimationRange("YBot_Run");

    if(data.action == "walk"){
      scene.beginAnimation(player.skeleton, walkRange.from, walkRange.to, true);
    } else if(data.action == "run"){
      scene.beginAnimation(player.skeleton, runRange.from, runRange.to, true);
    } else {
      scene.beginAnimation(player.skeleton, idleRange.from, idleRange.to, true);
    }
  }
});

// 메시지
socket.on("chat", function(data) {
  let today = new Date();   
  let hours = ('0' + today.getHours()).slice(-2); 
  let minutes = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2); 
  $("#chat_content").append("<div><strong>" + data.from.name + " </strong>"+hours+":"+minutes+":"+seconds+"<p> " + data.msg + "</p></div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
});

function send() {
  var chat_msg =$('#chat_msg').val();
  if(chat_msg != '' && chat_msg != ' ' ){
    // 서버로 메시지를 전송한다.
    socket.emit("chat", { msg: chat_msg });
    let today = new Date();   
    let hours = ('0' + today.getHours()).slice(-2); 
    let minutes = ('0' + today.getMinutes()).slice(-2);
    var seconds = ('0' + today.getSeconds()).slice(-2); 
    $("#chat_content").append("<div><strong>" + $('#name').val() + " </strong>"+hours+":"+minutes+":"+seconds+"<p> " + chat_msg + "</p></div>");
    $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
    $('#chat_msg').val("");
  }
}
