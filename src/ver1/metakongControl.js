window.onload = function () {

    main();
}
var start = false;
var inputMap = {};

function main() {

    var canvas = document.getElementById("renderCanvas");

    // Low Poly Character with Blender Tutorial of Grant Abbitt: https://www.youtube.com/user/mediagabbitt
    // Character animations by Mixamo: https://www.mixamo.com/

    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);

    // Lights
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.6;
    light.specular = BABYLON.Color3.Black();

    var light2 = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(0, -0.5, -1.0), scene);
    light2.position = new BABYLON.Vector3(0, 5, 5);

    var ground = createGround(scene);
    loadHVGirl(scene, engine, canvas); // VGirl ver1 

}

function createGround(scene) {

    // Skybox
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = BABYLON.Color3.Black()
    skyboxMaterial.specularColor = BABYLON.Color3.Black();
    skybox.material = skyboxMaterial;

    // camp ground
    BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/sehong90/metakong/raw/master/meshes/autumn_forest_camp/", "forest_camp.gltf", scene, function (newMeshes, particleSystems, skeletons, animationGroups) {
        var hero = newMeshes[0];

        //Scale the model down        
        hero.scaling.scaleInPlace(1.7);
        hero.rotation = new BABYLON.Vector3(0, Math.PI / 2, 0);
    });

}

function loadHVGirl(scene, engine, canvas) {

    // GUI
    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    var instructions = new BABYLON.GUI.TextBlock();
    instructions.text = "Move w/ WASD keys, B for Samba, look with the mouse";
    instructions.color = "white";
    instructions.fontSize = 16;
    instructions.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
    instructions.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM
    advancedTexture.addControl(instructions);

    // Keyboard events
    var keysLeft = [37, 65]; // "ArrowLeft", "A", "a", "ㅁ"
    var keysRight = [39, 68]; // "ArrowRight", "D", "d", "ㅇ"
    var keysForwards = [38, 87]; // "ArrowUp", "W", "w", "ㅉ", "ㅈ"
    var keysBackwards = [40, 83]; // "ArrowDown", "S", "s", "ㄴ"
    var keysSpeedModifier = [16]; // "Shift"
    var keysSamba = [66]; // "b"

    scene.actionManager = new BABYLON.ActionManager(scene);
    scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
        var t = evt.sourceEvent.keyCode;
        if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t)) {
            var key;
            -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysSamba.indexOf(t) && (key = "b");
            inputMap[key] = evt.sourceEvent.type == "keydown";
        }
    }));
    scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {
        var t = evt.sourceEvent.keyCode;
        if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t)) {
            var key;
            -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysSamba.indexOf(t) && (key = "b");
            inputMap[key] = evt.sourceEvent.type == "keydown";
        }
    }));

    // Load hero character
    BABYLON.SceneLoader.ImportMesh("", "https://assets.babylonjs.com/meshes/", "HVGirl.glb", scene, function (newMeshes, particleSystems, skeletons, animationGroups) {

        var hero = newMeshes[0];
        let localDirection = BABYLON.Vector3.Zero();

        //Scale the model down        
        hero.scaling.scaleInPlace(0.1);
        hero.position.set(3, 0, -2);
        hero.rotation = new BABYLON.Vector3(0, 0, 0);

        //Lock camera on the character 
        //camera.target = hero;
        // Camera
        var alpha = hero.rotation.y - Math.PI / 2;
        var beta = Math.PI;
        var target = new BABYLON.Vector3(hero.position.x, hero.position.y + 1.5, hero.position.z);

        //var camera = new BABYLON.ArcRotateCamera("camera", Math.PI, Math.PI, 5, new BABYLON.Vector3(0, 0, 0), scene);

        var camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", alpha, beta, 5, target, scene);
        scene.activeCamera = camera;
        scene.activeCamera.attachControl(canvas, true);

        camera.wheelPrecision = 15;
        camera.checkCollisions = false;

        camera.lowerRadiusLimit = 2;
        camera.upperRadiusLimit = 20;
        camera.lowerBetaLimit = -0.1;
        camera.upperBetaLimit = (Math.PI / 2) * 0.95;
        camera.wheelDeltaPercentage = 0.01;
        camera.inputs.remove(camera.inputs.attached.keyboard);

        scene.registerBeforeRender(function () {
            camera.target.copyFrom(hero.position);
            camera.target.y += 2;
        });

        //Hero character variables 
        var heroSpeed = 0.03;
        var heroSpeedBackwards = 0.01;
        var heroRotationSpeed = 0.1;

        var animating = true;

        const walkAnim = scene.getAnimationGroupByName("Walking");
        const walkBackAnim = scene.getAnimationGroupByName("WalkingBack");
        const idleAnim = scene.getAnimationGroupByName("Idle");
        const sambaAnim = scene.getAnimationGroupByName("Samba");

        //Rendering loop (executed for everyframe)
        scene.onBeforeRenderObservable.add(() => {
            var keydown = false;
            //Manage the movements of the character (e.g. position, direction)
            if (inputMap["w"] && inputMap["a"]) {
                hero.rotation = new BABYLON.Vector3(0, Math.PI / 1.5, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["w"] && inputMap["d"]) {
                hero.rotation = new BABYLON.Vector3(0, - Math.PI / 1.5, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["s"] && inputMap["a"]) {
                hero.rotation = new BABYLON.Vector3(0, Math.PI / 3, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["s"] && inputMap["d"]) {
                hero.rotation = new BABYLON.Vector3(0, - Math.PI / 3, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["w"]) {
                hero.rotation = new BABYLON.Vector3(0, - Math.PI, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["s"]) {
                hero.rotation = new BABYLON.Vector3(0, 0, 0);
                hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                keydown = true;
            } else if (inputMap["a"]) {
                hero.rotation = new BABYLON.Vector3(0, Math.PI / 2, 0);
                hero.position.x -= heroSpeed;
                keydown = true;
            } else if (inputMap["d"]) {
                hero.rotation = new BABYLON.Vector3(0, - Math.PI / 2, 0);
                hero.position.x += heroSpeed;
                keydown = true;
            } else if (inputMap["b"]) {
                keydown = true;
            }

            if (inputMap["Shift"]) {
                heroSpeed = 0.2;
                heroSpeedBackwards = 0.1;
            }

            //Manage animations to be played  
            if (keydown) {
                if (!animating) {
                    animating = true;
                    if
                        (inputMap["b"]) {
                        //Samba!
                        sambaAnim.start(true, 1.0, sambaAnim.from, sambaAnim.to, false);
                    }
                    else {
                        //Walk
                        walkAnim.start(true, 1.0, walkAnim.from, walkAnim.to, false);
                    }
                }
            }
            else {
                heroSpeed = 0.03;
                heroSpeedBackwards = 0.01;

                if (animating) {
                    //Default animation is idle when no key is down     
                    idleAnim.start(true, 1.0, idleAnim.from, idleAnim.to, false);

                    //Stop all animations besides Idle Anim when no key is down
                    sambaAnim.stop();
                    walkAnim.stop();
                    walkBackAnim.stop();

                    //Ensure animation are played only once per rendering loop
                    animating = false;
                }
            }
        });

        engine.runRenderLoop(function () {
            scene.render();
        });
        // Resize
        window.addEventListener("resize", function () {
            engine.resize();
        });
    });
}
