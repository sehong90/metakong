var app = require("express")();
var http = require("http").createServer(app);
var io = require('socket.io')(http, { cors: { origin: "*" } });

var port = 3000;
http.listen(port, () => {
  console.log("listening on *:" + port);
  console.log("접속자 수 :" + io.engine.clientsCount);

});

// localhost:3000으로 서버에 접속하면 클라이언트로 전송한다
app.get('/', function (req, res) {
  res.send('<h1>Hello world</h1>');
});

var players = [];
var total = 0;
var streaming = {
  streamer: null,
  type: "",
  live: false,
  stream : null,
}

const createPlayer = (socketId, userName, position) => ({
  socketId,
  userName,
  position
});

function numPlayers() {
  return Object.keys(players).length;
}

// Tracking variables for the update loop
let stateChanged = false;
let isEmittingUpdates = false;
const stateUpdateInterval = 300;

function emitStateUpdateLoop() {
  isEmittingUpdates = true;
  // Reduce usage by only send state update if state has changed
  if (stateChanged) {
    stateChanged = false;
    io.emit("stateUpdate", players);
  }

  if (numPlayers() > 0) {
    setTimeout(emitStateUpdateLoop, stateUpdateInterval);
  } else {
    // Stop the setTimeout loop if there are no players left
    isEmittingUpdates = false;
  }
}

// connection event handler
// connection이 수립되면 event handler function의 인자로 socket인 들어온다
io.on('connection', function (socket) {
  // 클라이언트 접속
  socket.on('login', function (data) {
    total +=1;
    var name= "콩콩이"+total;
    console.log('Client logged-in:\n name:' + name + '\n userId: ' + socket.id);

    // socket에 클라이언트 정보를 저장한다
    socket.name = name;

    // Create a new player object
    newPlayer = createPlayer(socket.id, name, data);

    // 접속된 모든 클라이언트에게 메시지를 전송한다
    socket.emit('login', players, newPlayer);

    // Add the newly created player to game state.
    players.push({
      player: newPlayer
    }); 
    
    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('createPlayer', newPlayer);

    // 접속자 수 업데이트
    io.emit('clientsCount', io.engine.clientsCount);
    console.log("접속자 수 :" + io.engine.clientsCount);
    console.log('players.length:: ' + players.length);

  });

  // 클라이언트로부터의 메시지가 수신되면
  socket.on('chat', function (data) {
    console.log('[MSG] %s: %s', socket.name, data.msg);

    var msg = {
      from: {
        name: socket.name
      },
      msg: data.msg
    };

    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('chat', msg);

  });

  socket.on("userNameUpdate", function(user) {
    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == user.socketId) {
        players[i].player.userName=user.userName;
        // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
        socket.broadcast.emit('userNameUpdate', players[i].player);
        break;
      }
    } 
  });

  socket.on("positionUpdate", function(positionData) {
    stateChanged = true;

    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == positionData.socketId) {
        players[i].player.position=positionData.position;
        break;
      }
    } 

    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('positionUpdate', positionData);
  });

  socket.on("animationUpdate", function(data) {
    stateChanged = true;
    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('animationUpdate', data);
  });

  socket.on("mainScreenStream", function (data) {
    console.log("누가 방송한다!");
    console.log(data.stream);

    streaming.streamer=data.socketId;
    streaming.type=data.type;
    streaming.live=true;
    streaming.stream=data.stream;

    socket.broadcast.emit('mainScreenStream', data);
  });

  // force client disconnect from server
  socket.on('forceDisconnect', function () {
    socket.disconnect();
  })

  socket.on('disconnect', function (data) {
    console.log('user disconnected: ' + socket.id);

    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == socket.id) {
        socket.broadcast.emit('disconnectPlayer', players);
        players.splice(i, 1);
        
        // 접속자 수 업데이트
        io.emit('clientsCount', io.engine.clientsCount);
        console.log("접속자 수 :" + io.engine.clientsCount);
        console.log('players.length:: ' + players.length);

        if (players.length == 0 ) total = 0;
        break;
      }
    }
  });
});

/*
// 접속된 모든 클라이언트에게 메시지를 전송한다
io.emit('event_name', msg);

// 메시지를 전송한 클라이언트에게만 메시지를 전송한다
socket.emit('event_name', msg);

// 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
socket.broadcast.emit('event_name', msg);

// 특정 클라이언트에게만 메시지를 전송한다
io.to(id).emit('event_name', data);
*/