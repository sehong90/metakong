var app = require("express")();
var http = require("http").createServer(app);
var io = require('socket.io')(http, { cors: { origin: "*" } });
let wrtc = require("wrtc");

var port = 3000;
http.listen(port, () => {
  console.log("listening on *:" + port+"\n\n");
  console.log("접속자 수 :" + io.engine.clientsCount);

});

// localhost:3000으로 서버에 접속하면 클라이언트로 전송한다
app.get('/', function (req, res) {
  res.send('<h1>Hello world</h1>');
});

var room = 'metakong';

var players = [];
var total = 0;

const createPlayer = (socketId, userName, position_x, position_z, avatarState) => ({
  socketId,
  userName,
  position_x,
  position_z,
  avatarState
});

const mainStreaming = (socketId, type, streamer, url) => ({
  socketId,
  type,
  streamer,
  url
});

// connection event handler
// connection이 수립되면 event handler function의 인자로 socket인 들어온다
io.on('connection', function (socket) {
  // 클라이언트 접속
  socket.on('join', function (data) {
    total +=1;
    var name= "콩콩이"+total;
    console.log('[Client logged-in]\n name:' + name + '\n userId: ' + socket.id);

    // socket에 클라이언트 정보를 저장한다
    socket.name = name;

    socket.join(room);

    // Create a new player object
    newPlayer = createPlayer(socket.id, name, data.position_x, data.position_z, 'avatar');

    // 뉴비에게 답장
    socket.emit('join', players, newPlayer);
/*
    // 메인 스크린 영상
    if (mainStreaming.type == 'webCam'){
      socket.emit('streamWebcam', {socketId : mainStreaming.socketId, streamer :mainStreaming.streamer });
    } else if (mainStreaming.type == 'url'){
      socket.emit('streamURL', {socketId : mainStreaming.socketId, streamer :mainStreaming.streamer, url :mainStreaming.url });
    }else if (mainStreaming.type == 'screen'){
      socket.emit('streamScreen', {socketId : mainStreaming.socketId, streamer :mainStreaming.streamer });
    }
*/
    // Add the newly created player to game state.
    players.push({
      player: newPlayer
    }); 
    
    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    // socket.broadcast.emit('createPlayer', newPlayer);

    // 접속자 수 업데이트
    io.emit('clientsCount', players.length);
    console.log("접속자 수 :" + io.engine.clientsCount);
    console.log('players.length:: ' + players.length);

  });

  socket.on('ipaddr', function() {
    var ifaces = os.networkInterfaces();
    for (var dev in ifaces) {
      ifaces[dev].forEach(function(details) {
        if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
          socket.emit('ipaddr', details.address);
        }
      });
    }
  });

  socket.on('bye', function(){
    console.log('received bye');
  });

  // 클라이언트로부터의 메시지가 수신되면
  socket.on('chat', function (data) {
    console.log('[MSG] %s: %s', data.userName, data.msg);
    var msg = {
      from: {
        name: data.userName
      },
      msg: data.msg
    };

    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('chat', msg);
  });

  function log() {
    var array = ['Message from server:'];
    array.push.apply(array, arguments);
    //socket.emit('log', array);
  }

  socket.on('message', function(message) {
    log('Client said: ', message);
    // for a real app, would be room-only (not broadcast)

    if (message.type === 'got user media'){
      for (var i = 0; i < players.length; i++) {
        if (players[i].player.socketId == message.socketId) {
          players[i].player.streamId=message.streamId;
        }
      } 
    }
    socket.broadcast.emit('message', message);
  });

  socket.on("playerInfo", function(data) {
    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == data.socketId) {
        socket.emit('createPlayer', players[i].player);
        break;
      }
    } 
  });

  socket.on("userNameChange", function(user) {
    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == user.socketId) {
        players[i].player.userName=user.userName;
        // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
        socket.broadcast.emit('userNameChange', players[i].player);
        break;
      }
    } 
  });

  socket.on("positionUpdate", function(positionData) {
    stateChanged = true;

    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == positionData.socketId) {
        players[i].player.position_x=positionData.position_x;
        players[i].player.position_z=positionData.position_z;
        break;
      }
    } 

    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('positionUpdate', positionData);
  });

  socket.on("animationUpdate", function(data) {
    stateChanged = true;

    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == data.socketId) {
        players[i].player.position_x=data.position_x;
        players[i].player.position_z=data.position_z;
        break;
      }
    } 
    
    // 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
    socket.broadcast.emit('animationUpdate', data);
  });

  socket.on("userAvatarChange", function(data) {
    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == data.socketId) {
        players[i].player.avatarState = data.avatarState;
        break;
      }
    } 
    socket.broadcast.emit('userAvatarChange', data);
  });

  socket.on('streamWebcam', function (data) {
    mainStreaming.socketId=data.socketId;
    mainStreaming.type='webCam';
    mainStreaming.streamer=data.streamer;
    socket.broadcast.emit('streamWebcam', data);
  });
  socket.on('streamScreen', function (data) {
    mainStreaming.socketId=data.socketId;
    mainStreaming.type='screen';
    mainStreaming.streamer=data.streamer;
    socket.broadcast.emit('streamScreen', data);
  });
  socket.on('streamURL', function (data) {
    mainStreaming.socketId=data.socketId;
    mainStreaming.type='url';
    mainStreaming.streamer=data.streamer;
    mainStreaming.url=data.url;
    socket.broadcast.emit('streamURL', data);
  });
  socket.on('stopStreaming', function (data) {
    if(mainStreaming.socketId == data && mainStreaming.type != 'stop'){
      mainStreaming.type='stop';
      socket.broadcast.emit('stopStreaming', {});
    }
  });

  // force client disconnect from server
  socket.on('forceDisconnect', function () {
    socket.disconnect();
  })

  socket.on('disconnect', function (data) {
    console.log('user disconnected: ' + socket.id);

    io.emit('disconnectPlayer', socket.id);
    // 접속자 수 업데이트

    if(mainStreaming.socketId == socket.id && mainStreaming.type != 'stop'){
      mainStreaming.type='stop';
      io.emit('stopStreaming', {});
    }

    if (players.length == 0 ) {
      total = 0;
      mainStreaming.type='stop';
    }

    for (var i = 0; i < players.length; i++) {
      if (players[i].player.socketId == socket.id) {
        players.splice(i, 1);
        break;
      }
    }
    io.emit('clientsCount', io.engine.clientsCount);
    console.log("접속자 수 :" + io.engine.clientsCount);
    console.log('players.length:: ' + players.length);
  });
});

/*
// 접속된 모든 클라이언트에게 메시지를 전송한다
io.emit('event_name', msg);

// 메시지를 전송한 클라이언트에게만 메시지를 전송한다
socket.emit('event_name', msg);

// 메시지를 전송한 클라이언트를 제외한 모든 클라이언트에게 메시지를 전송한다
socket.broadcast.emit('event_name', msg);

// 특정 클라이언트에게만 메시지를 전송한다
io.to(id).emit('event_name', data);
*/