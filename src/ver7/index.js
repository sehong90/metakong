var inputMap = {}; // 키보드 이벤트 

// 아바타 정보
var player_info = {
    position: new BABYLON.Vector3(3, 1.2, 35), // 기본 생성 위치
    //position: new BABYLON.Vector3(36, 1.2, -32), // 캠핑장 스크린 위치
    y: 0.077, // 아바타-캠 기본 높이
    alpha: Math.PI, // 시각
    speed: 0.018, // 속도
    name: "",
    nameTag: null,
    avatarState: "avatar",
    player: null,
    player_cam: null,
}

// babylon 엔진 세팅
var canvas = document.getElementById("renderCanvas");
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
var css3DRenderer;
var css3DRenderer_yn=true;
var diffuseTexture;

$(document).ready(function () {

    window.addEventListener("resize", function () {
        engine.resize();
    });
    window.onblur = function(){
        inputMap = {};
    }

    var keysLeft = [37, 65]; // "ArrowLeft", "A", "a", "ㅁ"
    var keysRight = [39, 68]; // "ArrowRight", "D", "d", "ㅇ"
    var keysForwards = [38, 87]; // "ArrowUp", "W", "w", "ㅉ", "ㅈ"
    var keysBackwards = [40, 83]; // "ArrowDown", "S", "s", "ㄴ"
    var keysSpeedModifier = [16]; // "Shift"
    var keysSamba = [66]; // "b"
    var keysDance = [81]; // "q"
    var keysSit = [67]; // "c"
    
    window.addEventListener("keydown", function(e) {
        const t = e.keyCode;
        focusEle = document.activeElement;
        if (document.getElementById('chat_msg') != focusEle && document.getElementById('name') != focusEle) {
            if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t) || -1 !== keysDance.indexOf(t) || -1 !== keysSit.indexOf(t)) {
                var key;
                -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysDance.indexOf(t) ? key = "q" : -1 !== keysSit.indexOf(t) ? key = "c" : -1 !== keysSamba.indexOf(t) && (key = "b");
                inputMap[key] = e.type == "keydown";
            }
        }
    });

    window.addEventListener("keyup", function(e) {
        const t = e.keyCode;
        focusEle = document.activeElement;
        if (document.getElementById('chat_msg') != focusEle && document.getElementById('name') != focusEle) {
            if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t) || -1 !== keysDance.indexOf(t) || -1 !== keysSit.indexOf(t)) {
                var key;
                -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysDance.indexOf(t) ? key = "q" : -1 !== keysSit.indexOf(t) ? key = "c" : -1 !== keysSamba.indexOf(t) && (key = "b");
                inputMap[key] = e.type == "keydown";
            }
        }
    });
});
function main_start() {

    scene.enablePhysics(); // 물리 엔진 활성화
    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0); 
    //scene.debugLayer.show({ embedMode: true }); // babylon 디버그
    
    // 필수 Lights - 햇빛
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.6;
    light.specular = BABYLON.Color3.Black();

    // 그라운드 생성
    initGround();

    // 그라운드 이벤트 
    // initGroundEvent();

    // 내 아바타 생성
    initPlayer();

    // 카메라 생성
    initCameraSetting();

    metakongTexture = new BABYLON.Texture("https://bitbucket.org/sehong90/metakong/raw/master/src/ver5/assets/img/metakong.png", scene);
    
    // 캠프 스테이지 메인 스크린 생성
    initMainScreen('');

    // 캠프 스테이지 버튼 생성
     initMainScreenBtn();

    // GUI - 화면 아래 안내문구
    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");
    var instructions = new BABYLON.GUI.TextBlock("instructions");
    instructions.text = "Move wasd keys, look with the mouse / Dance q / Sit c";
    instructions.color = "white";
    instructions.fontSize = 16;
    instructions.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
    instructions.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM
    advancedTexture.addControl(instructions);

    engine.runRenderLoop(function () {
        scene.render();
    });

}

// ----------------- init set -------------------
// 그라운드 생성
function initGround() {

    // Skybox 
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = BABYLON.Color3.Black()
    skyboxMaterial.specularColor = BABYLON.Color3.Black();
    skybox.material = skyboxMaterial;

    // Ground
    BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/sehong90/metakong/raw/master/src/ver6/assets/map/", "metakong_map.gltf", scene, function (newMeshes) {
        var ground = newMeshes[0];
        ground.id = "ground";
        ground.name = "ground";
        ground.scaling = new BABYLON.Vector3(-0.03, 0.03, -0.03);
        ground.rotation = new BABYLON.Vector3(0, Math.PI, 0);
        ground.checkCollisions = true;
    });

    // ----------------- 투명 객체 만들기 -------------------
    // 투명 설정
    const clearMaterial = new BABYLON.StandardMaterial("clearMaterial", scene);
    clearMaterial.alpha = 0;

    // 벽 바닥 
    const rise = 2;
    const diamInner = 112;
    const iWidth = diamInner * .15
    const diamOuter = diamInner + iWidth / 2

    const iFloor = BABYLON.MeshBuilder.CreateDisc("floor_", { radius: diamOuter / 2 - iWidth / 4 }, scene)
    const mFloor = new BABYLON.StandardMaterial("ifloor", scene)
    iFloor.rotation.x = Math.PI / 2;
    mFloor.material = clearMaterial;
    iFloor.material = clearMaterial;

    // 벽  
    const iOuter = BABYLON.MeshBuilder.CreateCylinder("iOuter", { diameter: diamOuter, height: rise }, scene)
    iOuter.position.y = rise / 2
    const iInner = BABYLON.MeshBuilder.CreateTube(
        "inner",
        {
            path: [new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, rise, 0)],
            radius: diamInner / 2,
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
        },
        scene
    )
    const outerCSG = BABYLON.CSG.FromMesh(iOuter)
    const innerCSG = BABYLON.CSG.FromMesh(iInner)
    const iRingCSG = outerCSG.subtract(innerCSG)
    const iRing = iRingCSG.toMesh("wall_", null, scene)
    iInner.dispose()
    iOuter.dispose()
    scene.removeMesh(iInner)
    scene.removeMesh(iOuter)
    scene.removeMesh(iRingCSG)

    iRing.checkCollisions = true;
    iRing.material = clearMaterial;

    // 중앙 화단 
    const cylinder = BABYLON.MeshBuilder.CreateCylinder("campfire_", { diameter: 34, height: 4 });
    cylinder.checkCollisions = true;
    cylinder.material = clearMaterial;

    // 나무 더미1
    const treebox1 = BABYLON.MeshBuilder.CreateBox("treebox1_", { width: 2, depth: 2.2, height: 4 });
    treebox1.checkCollisions = true;
    treebox1.position.set(-2.8, 0, 0);
    treebox1.rotation = new BABYLON.Vector3(0, -Math.PI / 8, 0);
    treebox1.material = clearMaterial;

}
// 그라운드 이벤트 생성
function initGroundEvent() {
    var npc1 = scene.getMeshByName("NPC SIZE.1");
    npc1.actionManager = new BABYLON.ActionManager(scene);
    npc1.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                //console.log("ㅡㄹ릭");
            })
    );
}

// 내 아바타 생성
function initPlayer() {

    BABYLON.SceneLoader.ImportMesh("girl", "https://bitbucket.org/sehong90/metakong/raw/master/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        player = meshes[0];
        player.scaling.scaleInPlace(0.5);
        player.name = "girl_" + socket.id;
        player._children[0]._position._y = -2.25;
        player.position = player_info.position;
        player.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);
        
        skeleton = skeletons[0];
        skeleton.name = "girl_" + socket.id;

        Cylinder1 = scene.getMaterialByName("Cylinder");
        Cylinder1.name = "Cylinder_"+socket.id;
        Cylinder2 = scene.getTextureByName("Cylinder (Emissive)");
        Cylinder2.name = "Cylinder_"+socket.id;

        // 아바타 애니메이션 설정
        var idleRange = animationGroups[3];
        var walkRange = animationGroups[4];
        var runRange = animationGroups[1];
        var sitRange = animationGroups[2];
        var danceRange = animationGroups[0];

        idleRange.name="idleRange_"+socket.id;
        walkRange.name="walkRange_"+socket.id;
        runRange.name="runRange_"+socket.id;
        sitRange.name="sitRange_"+socket.id;
        danceRange.name="danceRange_"+socket.id;
        if (idleRange) idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);

        player_info.player = player;
        initPlayerCam();

        var animating = true;
        // 아바타 무빙
        scene.onBeforeRenderObservable.add(() => {
            var keydown = false;
            player_info.alpha = Math.PI - (camera.alpha - Math.PI / 2);
            if (inputMap["w"] && inputMap["a"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 1.5), 0);
                keydown = true;
            } else if (inputMap["w"] && inputMap["d"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 1.5), 0);
                keydown = true;
            } else if (inputMap["s"] && inputMap["a"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 3), 0);
                keydown = true;
            } else if (inputMap["s"] && inputMap["d"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 3), 0);
                keydown = true;
            } else if (inputMap["w"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + Math.PI, 0);
                keydown = true;
            } else if (inputMap["s"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);
                keydown = true;
            } else if (inputMap["a"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 2), 0);
                keydown = true;
            } else if (inputMap["d"]) {
                player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 2), 0);
                keydown = true;
            } else if (inputMap["q"] || inputMap["c"]) {
                keydown = true;
            }

            if (keydown) {
                if (!(inputMap["q"] || inputMap["c"])) {
                    player_info.player.moveWithCollisions(player_info.player.forward.scaleInPlace(player_info.speed));
                }
                
                // 소켓에 아바타 위치 전송
                /*socket.emit("positionUpdate", {
                    socketId: socket.id,
                    position_x: player.position.x,
                    position_z: player.position.z,
                    rotation: player.rotation.y
                });*/
                if (!animating) {
                    animating = true;
                    if (inputMap["Shift"]) {
                        player_info.speed = 0.055;
                        runRange.start(true, 1.0, runRange.from, runRange.to, false);
                        // 소켓에 아바타 애니메이션 전송- 뛰는중!
                        player_rotation_y = player_info.player.rotation.y;
                        player_animation = 'runRange';
                        player_speed = 0.04;
                    } else if (inputMap["q"]) {
                        danceRange.start(true, 0.5, danceRange.from, danceRange.to, false);
                        player_rotation_y = player_info.player.rotation.y;
                        player_animation = 'danceRange';
                        player_speed = 0.01;
                    } else if (inputMap["c"]) {
                        sitRange.start(true, 1.0, sitRange.from, sitRange.to, false);
                        player_rotation_y = player_info.player.rotation.y;
                        player_animation = 'sitRange';
                        player_speed = 0.01;
                    } else {
                        walkRange.start(true, 1.0, walkRange.from, walkRange.to, false);
                        player_rotation_y = player_info.player.rotation.y;
                        player_animation = 'walkRange';
                        player_speed = 0.01;
                    }

                    socket.emit("animationUpdate", {
                        socketId: socket.id,
                        animation: player_animation,
                        speed: player_speed,
                        rotation: player_rotation_y,
                        position_x : player_info.player.position.x,
                        position_z : player_info.player.position.z
                    });
                } else {
                    if (player_info.player.rotation.y != player_rotation_y){
                        player_rotation_y = player_info.player.rotation.y;
                        socket.emit("animationUpdate", {
                            socketId: socket.id,
                            animation: player_animation,
                            speed: player_speed,
                            rotation: player_rotation_y,
                            position_x : player_info.player.position.x,
                            position_z : player_info.player.position.z
                        });
                    }
                }
            } else {
                player_info.speed = 0.018;
                
                if (animating) {
                    idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);
                    sitRange.stop();
                    danceRange.stop();
                    runRange.stop();
                    walkRange.stop();

                    animating = false;
                    // 소켓에 아바타 애니메이션 전송- 쉬는중!
                    socket.emit("animationUpdate", {
                        socketId: socket.id,
                        animation: 'idleRange',
                        speed: 0.018,
                        rotation: player_info.player.rotation.y,
                        position_x : player_info.player.position.x,
                        position_z : player_info.player.position.z
                    });
                }
            }
            try{
                player_info.nameTag.position.x = player.position.x;
                player_info.nameTag.position.z = player.position.z;
                player_info.nameTag.position.y = 1.8;
            } catch{}

            if(player_info.avatarState == "avatar"){
                player_info.player.setEnabled(true);
                player_info.player_cam.setEnabled(false);
            } else {
                player_info.player_cam.setEnabled(true);
                player_info.player.setEnabled(false);
            }
        });
        
    });
}
function initPlayerCam(){

    try{
        scene.removeMesh(scene.getMeshByName("webCam_" + socket.id));
        scene.getMaterialByName("playerCamMat_" + socket.id).dispose();
        scene.getTextureByName("playerCamVideo_" + socket.id).dispose();
    } catch{

    }
    // 아바타 웹캠
    playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + socket.id, scene);
    video = document.getElementById('localVideo');
    videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + socket.id, video, scene, true, true);    
    
    // 원형 plane 생성
    player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + socket.id, {});
    player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    player_disc.position = player_info.position;

    // plane에 캠 영상 적용
    playerCamMat.backFaceCulling = false;
    playerCamMat.diffuseTexture = videoTexture;
    playerCamMat.emissiveColor = BABYLON.Color3.White();
    player_disc.material = playerCamMat;

    player_info.player_cam = player_disc;

}

// 멤버 아바타 생성
// 기존 접속 아바타 생성 + 새로 접속한 아바타 생성
// 네임태그도 함께 생성
function createPlayer(data) {
   // 아바타 생성
    BABYLON.SceneLoader.ImportMesh("girl", "https://bitbucket.org/sehong90/metakong/raw/master/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {

        var playerA = meshes[0];
        playerA.scaling.scaleInPlace(0.5);
        playerA.name = "girl_" + data.socketId;
        playerA._children[0]._position._y = -2.25; // 캠 영상과 기준 좌표 맞추기 위해 추가
        playerA.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);
        playerA.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);

        skeleton = skeletons[0];
        skeleton.name = "girl_" + data.socketId;  

        Cylinder1 = scene.getMaterialByName("Cylinder");
        Cylinder1.name = "Cylinder_"+data.socketId;
        Cylinder2 = scene.getTextureByName("Cylinder (Emissive)");
        Cylinder2.name = "Cylinder_"+data.socketId;

        var idleRange = animationGroups[3];
        var walkRange = animationGroups[4];
        var runRange = animationGroups[1];
        var sitRange = animationGroups[2];
        var danceRange = animationGroups[0];
        idleRange.name="idleRange_"+data.socketId;
        walkRange.name="walkRange_"+data.socketId;
        runRange.name="runRange_"+data.socketId;
        sitRange.name="sitRange_"+data.socketId;
        danceRange.name="danceRange_"+data.socketId;

        idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);
        video = document.getElementById('remoteVideo_' + data.socketId);

        videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + data.socketId, video, scene, false);
        playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + data.socketId, scene);

        // 원형 plane 생성
        player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + data.socketId, {});
        player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
        player_disc.scaling= new BABYLON.Vector3(1, -1, 1);
        
        // plane에 캠 영상 적용
        playerCamMat.backFaceCulling = false;
        playerCamMat.diffuseTexture = videoTexture;
        playerCamMat.emissiveColor = BABYLON.Color3.White();
        player_disc.material = playerCamMat;

        player_disc.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);
        playerA.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);

        // 아바타 네임태그
        namePlaneA = setNameLabel(data, data.userName);
        
        if(data.avatarState == "avatar"){
            playerA.setEnabled(true);
            player_disc.setEnabled(false);
        } else {
            player_disc.setEnabled(true);
            playerA.setEnabled(false);
        }

        players.push({
            socketId: data.socketId,
            avatarState : data.avatarState,
            animation : 'idleRange',
            speed: '0.01',
            userName: data.userName,
            position_x : data.position_x,
            position_z : data.position_z,
            player: playerA,
            player_cam: player_disc,
            nameTag: namePlaneA
        });

        scene.onBeforeRenderObservable.add(() => { 
            if(animationUpdate_index>-1){
                try{
                    if (players[animationUpdate_index].animation != 'idleRange'  && players[animationUpdate_index].animation != 'sitRange' && players[animationUpdate_index].animation != 'danceRange'){
                        players[animationUpdate_index].player.moveWithCollisions(players[animationUpdate_index].player.forward.scaleInPlace(players[animationUpdate_index].speed));
                    } 
                    players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                    players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);
                } catch{
                }              
            }
        });
    });
}

// 아바타 닉네임 태그 생성
function setNameLabel(playerInfo, name) { 

    try{
        // 기존 닉네임 삭제
        scene.removeMesh(scene.getMeshByName("namePlane_" + playerInfo.socketId));
        scene.getMaterialByName("namePlane_" + playerInfo.socketId).dispose();
        scene.getTextureByName("nameText_" + playerInfo.socketId).dispose();
    } catch{}

    playerInfo.name=name;
    
    // 아바타 네임태그
    namePlane = BABYLON.MeshBuilder.CreatePlane('namePlane_' + playerInfo.socketId, { width: 0.5, height: 0.4 }, scene);
    namePlane.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    
    nameText = new BABYLON.GUI.TextBlock('nameText');
    nameText.text = name;
    nameText.fontSize = 60;
    if(playerInfo.socketId == socket.id){
        nameText.color = "yellow";
        namePlane.position = new BABYLON.Vector3(playerInfo.position_x, 1.8, playerInfo.position_z)
    }
    else{
        nameText.color = "white";
        namePlane.position = new BABYLON.Vector3(playerInfo.position_x, 1.8, playerInfo.position_z)
    }
    
    advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlane);
    advancedTexture.addControl(nameText);
    advancedTexture.name = "nameText_" + playerInfo.socketId;
    advancedTexture.renderScale = 0.3;
    namePlane.material.name = 'namePlane_' + playerInfo.socketId;

    if(playerInfo.socketId != socket.id){
        playerInfo.nameTag = namePlane;
    }

    // 설정창 닫기
    $('.modal').hide();
    document.getElementById('setting').innerText = "설정";
    return namePlane;
}

// 카메라 생성
function initCameraSetting() {

    var alpha = player_info.position.y + Math.PI / 8;
    var beta = Math.PI;
    var target = new BABYLON.Vector3(player_info.position.x, player_info.position.y, player_info.position.z);

    camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", alpha, beta, 5, target, scene);
    camera.checkCollisions = true;
    camera.wheelPrecision = 15;
    camera.lowerRadiusLimit = 2;
    camera.upperRadiusLimit = 80;
    camera.lowerBetaLimit = -0.1;
    camera.upperBetaLimit = (Math.PI / 2) * 0.95;
    camera.wheelDeltaPercentage = 0.01;
    camera.inputs.remove(camera.inputs.attached.keyboard);

    scene.activeCamera = camera;
    scene.activeCamera.attachControl(canvas, true);
    scene.registerBeforeRender(function () {
        // 카메라가 아바타 따라다니도록 세팅
        camera.target.copyFrom(player_info.position);
        camera.target.y = 1.2;
    });
}

// 메인 스크린 생성
function initMainScreen(screenType) {
    console.log("initMainScreen "+screenType)
    var planeOpts = {
        height: 4.2,
        width: 8.2,
        sideOrientation: BABYLON.Mesh.DOUBLESIDE
    };
    var ANote0Video = new BABYLON.MeshBuilder.CreatePlane("mainScreen", planeOpts, scene);
    ANote0Video.position = new BABYLON.Vector3(36.07, 3.15, -31.961);
    ANote0Video.rotation.y = ANote0Video.rotation.y + 2.88;
    ANote0Video.actionManager = new BABYLON.ActionManager(scene);
    ANote0Video.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                fullScreen();
            })
    );
    var mat = new BABYLON.StandardMaterial("mainScreenMat", scene);
    mat.emissiveColor = BABYLON.Color3.White();
    mat.diffuseTexture = metakongTexture;
    mat.emissiveColor = BABYLON.Color3.White();
    ANote0Video.material = mat;

    if(screenType == 'screen' || screenType == 'webCam'){
        mat.diffuseTexture.vScale =-1;   
        ANote0Video.scaling= new BABYLON.Vector3(1, -1, 1);
    } else {
        mat.diffuseTexture.vScale =1;   
        ANote0Video.scaling= new BABYLON.Vector3(1, 1, 1);
    }
}    
// 메인 스크린 버튼 생성
function initMainScreenBtn() {
   
    // 메인 스크린 발표자 라벨 생성
    streamerPlane = BABYLON.MeshBuilder.CreatePlane('mainScreenStreamer', { height: 2,width: 5 }, scene);
    streamerPlane.position = new BABYLON.Vector3(36.07,5, -31.941);
    streamerPlane.rotation.y = streamerPlane.rotation.y + 2.9;
    
    streamerText = new BABYLON.GUI.TextBlock('streamerText');
    streamerTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(streamerPlane);
    streamerTexture.name = "mainScreenStreamer";
    streamerTexture.renderScale = 0.4;
    streamerPlane.material.name = 'mainScreenStreamer';
    
    var label = new BABYLON.GUI.Rectangle("mainScreenLabel");
    label.background = "white"
    label.height = "80px";
    label.width = "200px";
    label.alpha = 0.8;
    label.cornerRadius = 20;
    label.thickness = 1;

    streamerTexture.addControl(label);
    label.addControl(streamerText);

    // 캠프 스테이지 스크린 공유 버튼
    mainScreenBtnMat = new BABYLON.StandardMaterial('mainScreenBtn', scene);
    mainScreenBtnMat.diffuseTexture = metakongTexture;
    mainScreenBtnMat.emissiveColor = BABYLON.Color3.White();
    mainScreenBtn = BABYLON.MeshBuilder.CreateDisc("mainScreenBtn", {radius: 0.4 });
    mainScreenBtn.position = new BABYLON.Vector3(31.5, 2.1, -33.5);  
    mainScreenBtn.rotation.z = mainScreenBtn.rotation.z - 3.14;
    mainScreenBtn.rotation.y = mainScreenBtn.rotation.y - 0.22;
    mainScreenBtn.scaling= new BABYLON.Vector3(1, 1, -1);
    mainScreenBtn.material = mainScreenBtnMat;
    mainScreenBtn.actionManager = new BABYLON.ActionManager(scene);
    mainScreenBtn.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                // 메인 스크린 선택- 설청 모달 띄움
                document.getElementById('setting').innerText="닫기";
                $('.name-box').hide()
                $('.url-box').show();
                $('.modal').show();
                $('.video-select').hide();
                var stream_state = document.getElementById('stream_state').value;
                if(stream_state == 'screen'){
                    $('.button-webcam').hide();
                }else {
                    $('.button-webcam').show();
                }
            })
    );
} 
// ----------------- init set end -------------------

// ----------------- event set ------------------
// 내 닉네임 변경 이벤트
function updateNameLabel() {
    // 닉네임 재설정
    var name = document.getElementById('name').value;
    player_info.nameTag = setNameLabel(player_info, name);

    // 소켓에 전달
    socket.emit("userNameChange", {
        socketId: socket.id,
        userName: name,
        position_x: player_info.player.position.x,
        position_z: player_info.player.position.z,
    });
}

// 아바타-캠 스위치 액션
function userAvatarChange(index) {
    if( players[index].avatarState == "avatar"){
        players[index].player.setEnabled(true);
        players[index].player_cam.setEnabled(false);
    } else {
        players[index].player_cam.setEnabled(true);
        players[index].player.setEnabled(false);
    }
}

// 메인 스크린 스트리밍 이벤트(로컬 세팅)
function localVideoStream(type) {
    if($('#stream_state').val() != 'stop'){
        alert('이미 사용중입니다.');    
        return;
    }

    console.log($('#stream_state').val());
    // 기존에 재생중이던 스크린 종료
    $('.button-webcam').show();
    scene.removeMesh(scene.getMeshByName("mainScreen"));
    scene.getMaterialByName("mainScreenMat").dispose();
    if($('#stream_state').val() == 'url'){
        removeDomNode("iframeContainer")
        removeDomNode("iframe-video")
        removeDomNode("cssContainer")
        removeDomNode("CSS3DRendererDom")
        scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
    } else if($('#stream_state').val() == 'webCam'){
        scene.getTextureByName("mainScreenVideo").dispose();
    } else if($('#stream_state').val() == 'screen'){
        scene.getTextureByName("mainScreenVideo").dispose();
        localStream.getTracks().forEach(track => track.stop())
        localStream = screanStream;
        var toggle = document.getElementById('toggle-mic');
        $('.toggle-mic-on').css('display','none');
        $('.toggle-mic-off').css('display','block');
        toggle.setAttribute('data', 'off');
        localStream.getAudioTracks()[0].enabled = false; 
        mic_switch = false;
        //localStream.getTracks().forEach(track => track.start())
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰', 
            'dest': 'all'
        }
        sendMessage(msg);
    }
    // 스크린 초기화
    initMainScreen(type);

    // 스트리밍 타입 세팅하고 시작 
    $('#stream_state').val(type);

    if(type == "url"){
        // 영상 url 공유
        var url = document.getElementById('url').value;
        if(url == ""){
            alert("URL 입력하시길");
        } else {
            // url 스트리밍 시작
            plane = scene.getMeshByName("mainScreen");

            css3DRenderer = setupRenderer(plane, url, scene);
            scene.onBeforeRenderObservable.add(() => {
                css3DRenderer.render(scene, scene.activeCamera)
            })
            streamerText.text = player_info.name;

            $('#stream_state').val(type)
            // 팝업 전체화면
            $('#remoteVideos').css('height','80%');
            $('#remotePlayerVideos').css('height','0');

            // 공유 중지 버튼
            $('.button-stop').css('display','block');

            // 소켓 통신
            socket.emit("streamURL", { socketId: socket.id, streamer : player_info.name, url: url });
        }
    } else if (type == "webCam"){
        // 스트리밍 시작
        const video =  document.getElementById("remoteVideo");
        video.srcObject = localStream;
        setTimeout(function() {
            mainScreenStream(video);
        },1000);
        streamerText.text = player_info.name;    

        $('#stream_state').val(type)
        // 팝업 전체화면
        $('#remoteVideos').css('height','80%');
        $('#remotePlayerVideos').css('height','0');
        
        // 공유 중지 버튼
        $('.button-stop').css('display','block');    
        
        // 소켓 통신
        socket.emit("streamWebcam", { socketId: socket.id, streamer : player_info.name });

    } else if(type == "screen"){
        // 웹캠 스트리밍 공유
        const video =  document.getElementById("remoteVideo");
        video.srcObject = localStream;
        setTimeout(function() {
            mainScreenStream(video);
        },1000);
        $('.button-stop').css('display','block');
        $('#remoteVideos').css('height','80%');
        $('#stream_state').val('screen')
        $('#remotePlayerVideos').css('height','0');
        
        streamerText.text = player_info.name;
        
        // 소켓 통신
        socket.emit("streamScreen", { socketId: socket.id, streamer : player_info.name });

        // 설정창 닫기
        $('.modal').hide(); 
        document.getElementById('setting').innerText="설정";
    }
    // 설정창 닫기
    $('.modal').hide(); 
    document.getElementById('setting').innerText="설정";
}

// 멤버 삭제
function removeAvatar(rmv_player, nameTag) {
     // 아바타 삭제
     try{
                
        rmv_player.player.dispose();
        scene.getSkeletonByName("girl_"+rmv_player.socketId).dispose();

        // 캠 plane 삭제
        scene.removeMesh(scene.getMeshByName("webCam_" + rmv_player.socketId));
        scene.getMaterialByName("playerCamMat_" + rmv_player.socketId).dispose();
        scene.getTextureByName("playerCamVideo_" + rmv_player.socketId).dispose();

        scene.getAnimationGroupByName("danceRange_"+rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("idleRange_"+rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("runRange_"+rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("sitRange_"+rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("walkRange_"+rmv_player.socketId).dispose()
        
        scene.getMaterialByName("Cylinder_"+rmv_player.socketId).dispose();
        scene.getTextureByName("Cylinder_"+rmv_player.socketId).dispose();

        if(nameTag){
            scene.getTextureByName("nameText_" + rmv_player.socketId).dispose();
            scene.getMaterialByName("namePlane_" + rmv_player.socketId).dispose();
            scene.removeMesh(scene.getMeshByName("namePlane_" + rmv_player.socketId));
        }

        //scene.removeMesh(scene.getMeshByName("girl_" + rmv_player.socketId));
        //scene.getMaterialByName("Cylinder").dispose();
        //scene.getTextureByName("Cylinder (Emissive)").dispose();
        /*
        rmv_player.player.subMeshes[0].getMaterial().dispose();
        rmv_player.player.subMeshes[1].getMaterial().dispose();
        rmv_player.player.material.dispose();
        rmv_player.player.skeleton.dispose();

        */
    } catch{
        
    }
}

// ----------------- event set end-------------------
// -------------- 영상 공유 func start----------------

function mainScreenStream(video) {
    mainScreenVideoTexture = new BABYLON.VideoTexture('mainScreenVideo', video, scene, true, true);
    mainScreenVideoTexture.emissiveColor = new BABYLON.Color3.White()
    plane = scene.getMeshByName("mainScreen");

    mat = scene.getMaterialByName("mainScreenMat");
    mat.diffuseTexture = mainScreenVideoTexture; 
    plane.material = mat;
    //mat.diffuseTexture.vScale =-1;  
}
function mainScreenStreamer(streamer) {
    
    // 발표자 이름 표시
    scene.removeMesh(scene.getMeshByName("mainScreenStreamer"));
    scene.getTextureByName("mainScreenStreamer").dispose();
    scene.getMaterialByName("mainScreenStreamer").dispose();

    streamerPlane = BABYLON.MeshBuilder.CreatePlane('mainScreenStreamer', { height: 2,width: 5 }, scene);
    streamerText = new BABYLON.GUI.TextBlock('streamerText');
    streamerTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(streamerPlane);
    streamerPlane.material.name = 'mainScreenStreamer';
    streamerPlane.position = new BABYLON.Vector3(36.07,5, -31.941);
    streamerPlane.rotation.y = streamerPlane.rotation.y + 2.9;
    
    var label = new BABYLON.GUI.Rectangle("mainScreenLabel");
    label.background = "white"
    label.height = "80px";
    label.width = "200px";
    label.alpha = 0.8;
    label.cornerRadius = 20;
    label.thickness = 1;

    streamerText.text = streamer;
    streamerText.color = "black";
    streamerText.fontSize = 30;

    streamerTexture.addControl(label);
    streamerTexture.name = "mainScreenStreamer";
    streamerTexture.renderScale = 0.4;
    label.addControl(streamerText);
}
function removeDomNode(id) {
    let node = document.getElementById(id);
    if (node && node.parentNode) {
        node.parentNode.removeChild(node);
    }
}
let videoWidth = 800
let videoHeight = 480
let tvThickness = .2

// 영상 공유 정지
function localStopStreaming(){
    scene.removeMesh(scene.getMeshByName("mainScreen"));
    scene.getMaterialByName("mainScreenMat").dispose();
    type = $('#stream_state').val();
    if (type == "screen"){
        scene.getTextureByName("mainScreenVideo").dispose();

        localStream.getTracks().forEach(track => track.stop())
        localStream = screanStream;
        var toggle = document.getElementById('toggle-mic');
        $('.toggle-mic-on').css('display','none');
        $('.toggle-mic-off').css('display','block');
        toggle.setAttribute('data', 'off');
        localStream.getAudioTracks()[0].enabled = false; 
        mic_switch = false;
        //localStream.getTracks().forEach(track => track.start())
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰', 
            'dest': 'all'
        }
        sendMessage(msg);
    } else if(type == "webCam"){
        scene.getTextureByName("mainScreenVideo").dispose();
    } else if(type == "url"){
        // 기존 스트리밍 삭제
        removeDomNode("iframeContainer")
        removeDomNode("iframe-video")
        removeDomNode("cssContainer")
        removeDomNode("CSS3DRendererDom")
        scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
    }

    socket.emit("stopStreaming", socket.id);

    streamerText.text = '';
    $('.modal').css('display','none');
    document.getElementById('setting').innerText="설정";
    $('.button-stop').css('display','none');
    $('#stream_state').val('stop');
    
    initMainScreen('');
    $('#stream_state').val('stop');
}
// 메인 스크린에 URL 영상 스트리밍
function setupRenderer(videoViewMesh, videoURL, scene) {

    let webGLContainer = document.getElementById('canvasZone')

    // 컨테이너 생성
    let css3DContainer = document.createElement('div')
    css3DContainer.id = 'cssContainer'
    css3DContainer.style.position = 'absolute'
    css3DContainer.style.width = '100%'
    css3DContainer.style.height = '100%'
    css3DContainer.style.zIndex = '-1'
    webGLContainer.insertBefore(css3DContainer, webGLContainer.firstChild)

    let css3DRenderer = new CSS3DRenderer()
    css3DContainer.appendChild(css3DRenderer.domElement)
    css3DRenderer.setSize(webGLContainer.offsetWidth, webGLContainer.offsetHeight)

    // 영상 실행할 iFrame 컨테이너 생성
    var iframeContainer = document.createElement('div')
    iframeContainer.style.width = videoWidth + 'px'
    iframeContainer.style.height = videoHeight + 'px'
    iframeContainer.style.backgroundColor = '#000'
    iframeContainer.id = "iframeContainer"

    // CSS Object 생성
    var CSSobject = new CSS3DObject(iframeContainer, scene)
    CSSobject.position.copyFrom(videoViewMesh.getAbsolutePosition())
    CSSobject.rotation.y = -videoViewMesh.rotation.y
    CSSobject.scaling.copyFrom(videoViewMesh.scaling)
    CSSobject.name ="mainScreenUrl";

    // iFrame 생성 
    var iframe = document.createElement('iframe')
    iframe.id = 'iframe-video'
    iframe.style.width = videoWidth + 'px'
    iframe.style.height = videoHeight + 'px'
    iframe.style.border = '0px'
    iframe.allow = 'autoplay'
    iframe.src = [videoURL.replace('watch?v=','embed/').replace('youtu.be/','www.youtube.com/embed/'), null, '?rel=0&enablejsapi=1&disablekb=1&autoplay=1&controls=0&fs=0&modestbranding=1'].join('')
    iframeContainer.appendChild(iframe)

    // material 생성
    let depthMask = new BABYLON.StandardMaterial('VideoViewMaterial', scene)
    depthMask.backFaceCulling = true
    videoViewMesh.material = depthMask

    // Render Video the mesh
    videoViewMesh.onBeforeRenderObservable.add(() => engine.setColorWrite(false))
    videoViewMesh.onAfterRenderObservable.add(() => engine.setColorWrite(true))

    // swap meshes to put mask first
    var videoPlaneIndex = scene.meshes.indexOf(videoViewMesh)
    scene.meshes[videoPlaneIndex] = scene.meshes[0]
    scene.meshes[0] = videoViewMesh

    return css3DRenderer
}

class CSS3DObject extends BABYLON.Mesh {
    constructor(element, scene) {
        super()
        this.element = element
		this.element.style.position = 'absolute'
		this.element.style.pointerEvents = 'auto'
    }
}

class CSS3DRenderer {
    constructor() {
		var matrix = new BABYLON.Matrix()

		this.cache = {
			camera: { fov: 0, style: '' },
			objects: new WeakMap()
		}

		var domElement = document.createElement( 'div' )
		domElement.style.overflow = 'hidden'

		this.domElement = domElement
		this.cameraElement = document.createElement( 'div' )
		this.isIE = (!!document['documentMode'] || /Edge/.test(navigator.userAgent) || /Edg/.test(navigator.userAgent))

		if (!this.isIE) {
			this.cameraElement.style.webkitTransformStyle = 'preserve-3d'
			this.cameraElement.style.transformStyle = 'preserve-3d'
		}
		this.cameraElement.style.pointerEvents = 'none'
		domElement.appendChild(this.cameraElement)
    }

    getSize() {
		return {
			width: this.width,
			height: this.height
		}
    }

	setSize(width, height) {
		this.width = width
		this.height = height
		this.widthHalf = this.width / 2
		this.heightHalf = this.height / 2

		this.domElement.style.width = width + 'px'
		this.domElement.style.height = height + 'px'

		this.cameraElement.style.width = width + 'px'
		this.cameraElement.style.height = height + 'px'
	}    

	epsilon(value) {
		return Math.abs(value) < 1e-10 ? 0 : value
	}

	getCameraCSSMatrix(matrix) {
		var elements = matrix.m

		return 'matrix3d(' +
			this.epsilon( elements[ 0 ] ) + ',' +
			this.epsilon( - elements[ 1 ] ) + ',' +
			this.epsilon( elements[ 2 ] ) + ',' +
			this.epsilon( elements[ 3 ] ) + ',' +
			this.epsilon( elements[ 4 ] ) + ',' +
			this.epsilon( - elements[ 5 ] ) + ',' +
			this.epsilon( elements[ 6 ] ) + ',' +
			this.epsilon( elements[ 7 ] ) + ',' +
			this.epsilon( elements[ 8 ] ) + ',' +
			this.epsilon( - elements[ 9 ] ) + ',' +
			this.epsilon( elements[ 10 ] ) + ',' +
			this.epsilon( elements[ 11 ] ) + ',' +
			this.epsilon( elements[ 12 ] ) + ',' +
			this.epsilon( - elements[ 13 ] ) + ',' +
			this.epsilon( elements[ 14 ] ) + ',' +
			this.epsilon( elements[ 15 ] ) +
		')'
	}    

	getObjectCSSMatrix(matrix, cameraCSSMatrix) {
		var elements = matrix.m;
		var matrix3d = 'matrix3d(' +
			this.epsilon( elements[ 0 ] ) + ',' +
			this.epsilon( elements[ 1 ] ) + ',' +
			this.epsilon( elements[ 2 ] ) + ',' +
			this.epsilon( elements[ 3 ] ) + ',' +
			this.epsilon( - elements[ 4 ] ) + ',' +
			this.epsilon( - elements[ 5 ] ) + ',' +
			this.epsilon( - elements[ 6 ] ) + ',' +
			this.epsilon( - elements[ 7 ] ) + ',' +
			this.epsilon( elements[ 8 ] ) + ',' +
			this.epsilon( elements[ 9 ] ) + ',' +
			this.epsilon( elements[ 10 ] ) + ',' +
			this.epsilon( elements[ 11 ] ) + ',' +
			this.epsilon( elements[ 12 ] ) + ',' +
			this.epsilon( elements[ 13 ] ) + ',' +
			this.epsilon( elements[ 14 ] ) + ',' +
			this.epsilon( elements[ 15 ] ) +
		')'

		if (this.isIE) {
			return 'translate(-50%,-50%)' +
				'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)' +
				cameraCSSMatrix +
				matrix3d;
		}
		return 'translate(-50%,-50%)' + matrix3d
	}    

	renderObject(object, scene, camera, cameraCSSMatrix ) {
        if (object instanceof CSS3DObject) {
            var style
			var objectMatrixWorld = object.getWorldMatrix().clone()
			var camMatrix = camera.getWorldMatrix()
			var innerMatrix = objectMatrixWorld.m

			// Set scaling
			const youtubeVideoWidth = 1.6
			const youtubeVideoHeight = 1.2

			innerMatrix[0] *= 0.01 / youtubeVideoWidth
			innerMatrix[2] *= 0.01 / youtubeVideoWidth
			innerMatrix[5] *= 0.01 / youtubeVideoHeight

			// Set position from camera
			innerMatrix[12] = -camMatrix.m[12] + object.position.x
			innerMatrix[13] = -camMatrix.m[13] + object.position.y
			innerMatrix[14] = camMatrix.m[14] - object.position.z
			innerMatrix[15] = camMatrix.m[15] * 0.00001

			objectMatrixWorld = BABYLON.Matrix.FromArray(innerMatrix)
            objectMatrixWorld = objectMatrixWorld.scale(100)
			style = this.getObjectCSSMatrix( objectMatrixWorld, cameraCSSMatrix)
            var element = object.element
            var cachedObject = this.cache.objects.get( object )

            if ( cachedObject === undefined || cachedObject.style !== style ) {

                element.style.webkitTransform = style
                element.style.transform = style

                var objectData = { style: style }

                this.cache.objects.set( object, objectData )
            }
            if ( element.parentNode !== this.cameraElement ) {
                this.cameraElement.appendChild( element )
            }

        } else if ( object instanceof BABYLON.Scene ) {
            for ( var i = 0, l = object.meshes.length; i < l; i ++ ) {
                this.renderObject( object.meshes[ i ], scene, camera, cameraCSSMatrix )
            }
        }
	}    

	render(scene, camera) {
        var projectionMatrix = camera.getProjectionMatrix()
		var fov = projectionMatrix.m[5] * this.heightHalf

		if (this.cache.camera.fov !== fov) {

			if (camera.mode == BABYLON.Camera.PERSPECTIVE_CAMERA ) {
				this.domElement.style.webkitPerspective = fov + 'px'
				this.domElement.style.perspective = fov + 'px'
			} else {
				this.domElement.style.webkitPerspective = ''
				this.domElement.style.perspective = ''
			}
			this.cache.camera.fov = fov
		}

		if ( camera.parent === null ) camera.computeWorldMatrix()

		var matrixWorld = camera.getWorldMatrix().clone()
		var rotation = matrixWorld.clone().getRotationMatrix().transpose()
		var innerMatrix = matrixWorld.m

		innerMatrix[1] = rotation.m[1]
		innerMatrix[2] = -rotation.m[2]
		innerMatrix[4] = -rotation.m[4]
		innerMatrix[6] = -rotation.m[6]
		innerMatrix[8] = -rotation.m[8]
		innerMatrix[9] = -rotation.m[9]

		matrixWorld = BABYLON.Matrix.FromArray(innerMatrix)

		var cameraCSSMatrix = 'translateZ(' + fov + 'px)' + this.getCameraCSSMatrix( matrixWorld )

		var style = cameraCSSMatrix + 'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)'

		if (this.cache.camera.style !== style && !this.isIE ) {
			this.cameraElement.style.webkitTransform = style
			this.cameraElement.style.transform = style
			this.cache.camera.style = style
		}

        this.renderObject(scene, scene, camera, cameraCSSMatrix )
	}  
}
// -------------- 영상 공유 func end----------------