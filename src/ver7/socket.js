// socket.io 서버에 접속한다
var socket = io("wss://metaforming.com:3000");
//var socket = io('http://localhost:5010');

var players = [];

var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var screanStream;
var pc;
var remoteStream;
var turnReady;
var peerConnections = {}; // key is uuid, values are peer connection object and user defined display name string

var streamPeer = null;
const peers = {};
var conn = null;

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  },
  {
    url: 'turn:numb.viagenie.ca',
    credential: 'muazkh',
    username: 'webrtc@live.com'
  }
  ]
};

var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

$(document).ready(function () {
  $('#chat_msg').keypress(function (evt) {
    if ((evt.keyCode || evt.which) == 13) {
      evt.preventDefault();
      send();
    }
  });
});

// 총 접속자 수
socket.on("clientsCount", function (data) {
  $("#clients_count").html("현재 접속자 수 : " + data);
});

// 유저 입장
socket.on("join", function (playerList, newPlayer) {

  $('#name').val(newPlayer.userName);
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + newPlayer.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  
  player_info.name = newPlayer.userName;
  player_info.socketId = newPlayer.socketId;
  player_info.nameTag = setNameLabel(newPlayer, newPlayer.userName);
    
  if (playerList.length == 0){
    isInitiator = true;
  }
});
// 다른 유저 입장
socket.on("createPlayer", function (data) {
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + data.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  createPlayer(data);
});

// 유저 닉네임 변경
socket.on("userNameChange", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      players[i].position_x = data.position_x;
      players[i].position_z = data.position_z;
      players[i].userName = data.userName;
      setNameLabel(players[i], data.userName);
      break;
    }
  }
});

// 유저 아바타 변경
socket.on("userAvatarChange", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      players[i].avatarState=data.avatarState;
      userAvatarChange(i, players[i]);
      break;
    }
  }
});

// 유저 이동
socket.on("positionUpdate", function (positionData) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == positionData.socketId) {
      index = i;
      break;
    }
  }
  if (index > -1) {
    let player = players[i];
    player.player.position.x = positionData.position_x;
    player.player.position.z = positionData.position_z;
    player.player_cam.position.x = positionData.position_x;
    player.player_cam.position.z = positionData.position_z;
    if(player.avatarState != "webcam"){
      player.player.rotation = new BABYLON.Vector3(0, positionData.rotation, 0);
    }
    player.nameTag.position = new BABYLON.Vector3(positionData.position_x, 1.8, positionData.position_z);
    
  }
});
animationUpdate_index = -1;
// 유저 액션 변경
socket.on("animationUpdate", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      animationUpdate_index = i;
      break;
    }
  }
  if(animationUpdate_index > -1){
    animation_name = players[animationUpdate_index].animation == null ? 'idleRange' : players[animationUpdate_index].animation;
    scene.getAnimationGroupByName(animation_name+"_"+data.socketId).stop();
    /*
    if(data.animation == 'idleRange'){
      console.log('서있음');
    } else if(data.animation == 'walkRange'){
      console.log('걷는중');
    } else if(data.animation == 'runRange'){
      console.log('뛰는중');
    } else if(data.animation == 'sitRange'){
      console.log('앉았음');
    } else if(data.animation == 'danceRange'){
      console.log('딴쓰딴쓰');
    }
    */
    players[animationUpdate_index].animation = data.animation;
    ani = scene.getAnimationGroupByName(data.animation+"_"+data.socketId);
    ani.start(true, 1.0, ani.from, ani.to, false);
    players[animationUpdate_index].player.rotation = new BABYLON.Vector3(0, data.rotation, 0);     
    players[animationUpdate_index].speed = data.speed;
    if(animationUpdate_index>-1){
      try{
        if (!(players[animationUpdate_index].animation != 'idleRange'  && players[animationUpdate_index].animation != 'sitRange' && players[animationUpdate_index].animation != 'danceRange')){
          players[animationUpdate_index].player.position= new BABYLON.Vector3(data.position_x, 1.2, data.position_z);   
        }
      } catch{
      }            
    }
  }
});

// 채팅
socket.on("chat", function (data) {
  let today = new Date();
  let hours = ('0' + today.getHours()).slice(-2);
  let minutes = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2);
  $("#chat_content").append("<div><strong>" + data.from.name + " </strong>" + hours + ":" + minutes + ":" + seconds + "<p> " + data.msg + "</p></div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
});

// 공용 스트리밍 
socket.on('streamWebcam', function (data) {
    // 공유 중지 버튼
    $('.button-stop').css('display','none'); 
     // 기존에 재생중이던 스크린 종료
    scene.removeMesh(scene.getMeshByName("mainScreen"));
    scene.getMaterialByName("mainScreenMat").dispose();
    if($('#stream_state').val() == 'url'){
      removeDomNode("iframeContainer")
      removeDomNode("iframe-video")
      removeDomNode("cssContainer")
      removeDomNode("CSS3DRendererDom")
      scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
    } else if($('#stream_state').val() == 'webCam' || $('#stream_state').val() == 'screen'){
      scene.getTextureByName("mainScreenVideo").dispose();
    }

    // 스크린 초기화
    initMainScreen('webCam');

     // 스트리밍 타입 세팅하고 시작 
   $('#stream_state').val('webCam');
    const video =  document.getElementById("remoteVideo_"+data.socketId);
    
    mainScreenStream(video);
    mainScreenStreamer(data.streamer);

    // 팝업 전체화면
    $('#remoteVideos').css('height','0');
    $('#remotePlayerVideos').css('height','80%');
  
});
socket.on('streamScreen', function (data) {
  // 공유 중지 버튼
  $('.button-stop').css('display','none');   
  // 기존에 재생중이던 스크린 종료
  scene.removeMesh(scene.getMeshByName("mainScreen"));
  scene.getMaterialByName("mainScreenMat").dispose();
  if($('#stream_state').val() == 'url'){
    removeDomNode("iframeContainer")
    removeDomNode("iframe-video")
    removeDomNode("cssContainer")
    removeDomNode("CSS3DRendererDom")
    scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
  } else if($('#stream_state').val() == 'webCam' || $('#stream_state').val() == 'screen'){
    scene.getTextureByName("mainScreenVideo").dispose();
  }
  
  // 스크린 초기화
  initMainScreen('screen');

  // 스트리밍 타입 세팅하고 시작 
  $('#stream_state').val('screen');
  const video =  document.getElementById("remoteVideo_"+data.socketId);
  
  setTimeout(function() {
    mainScreenStream(video);
  },3000);
  mainScreenStreamer(data.streamer);

  // 팝업 전체화면
  $('#remoteVideos').css('height','0');
  $('#remotePlayerVideos').css('height','80%');
});
socket.on('streamURL', function (data) {
    // 공유 중지 버튼
    $('.button-stop').css('display','none'); 
  // 기존에 재생중이던 스크린 종료
  scene.removeMesh(scene.getMeshByName("mainScreen"));
  scene.getMaterialByName("mainScreenMat").dispose();
  if($('#stream_state').val() == 'url'){
    removeDomNode("iframeContainer")
    removeDomNode("iframe-video")
    removeDomNode("cssContainer")
    removeDomNode("CSS3DRendererDom")
    scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
  } else if($('#stream_state').val() == 'webCam' || $('#stream_state').val() == 'screen'){
    scene.getTextureByName("mainScreenVideo").dispose();
  }

  // 스크린 초기화
  $('#stream_state').val('url');
  initMainScreen('url');

  // url 스트리밍 시작
  plane = scene.getMeshByName("mainScreen");

  css3DRenderer = setupRenderer(plane, data.url, scene);
  scene.onBeforeRenderObservable.add(() => {
      css3DRenderer.render(scene, scene.activeCamera)
  })

  mainScreenStreamer(data.streamer);

  // 팝업 전체화면
  $('#remoteVideos').css('height','0');
  $('#remotePlayerVideos').css('height','80%');
});

// 공용 스트리밍 종료
socket.on('stopStreaming', function (data) {
  
  type = $('#stream_state').val();
  scene.removeMesh(scene.getMeshByName("mainScreen"));
  scene.getMaterialByName("mainScreenMat").dispose();
  if (type == "screen"){
      scene.getTextureByName("mainScreenVideo").dispose();
  } else if(type == "webCam"){
      scene.getTextureByName("mainScreenVideo").dispose();
  } else if(type == "url"){
      // 기존 스트리밍 삭제
      removeDomNode("iframeContainer")
      removeDomNode("iframe-video")
      removeDomNode("cssContainer")
      removeDomNode("CSS3DRendererDom")
      scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
  }
  initMainScreen('');
  streamerText.text = '';
  $('.modal').css('display','none');
  document.getElementById('setting').innerText="설정";
  $('.button-stop').css('display','none');
  $('#stream_state').val('stop');
});

// 소켓 통신
socket.on('log', function (array) {
  console.log.apply(console, array);
});

function sendMessage(message) {
  //console.log('Client sending message: ', message);
  socket.emit('message', message);
}

// 소켓 메시지
socket.on("message", function (signal) {
  var socketId = signal.socketId;
  var localUuid = socket.id;

  // Ignore messages that are not for us or from ourselves
  if (socketId == localUuid || (signal.dest != localUuid && signal.dest != 'all')) return;
  
  try{
    if (signal.displayName && signal.dest == 'all') {
    // 새로 들어온 peer 연결 요청하는
    // set up peer connection object for a newcomer peer
    
    setUpPeer(socketId, signal.displayName);
    //serverConnection.send(JSON.stringify({ 'displayName': localDisplayName, 'uuid': localUuid, 'dest': peerUuid }));
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'displayName': '콩콩쓰', 
      'dest': socketId
    }
    // 요청 허가 답변 보낸다
    sendMessage(msg)
    
  } else if (signal.displayName && signal.dest == localUuid) {
    // initiate call if we are the newcomer peer
    // 요청 허가 답변 받는다 
    setUpPeer(socketId, signal.displayName, true);
    
  } else if (signal.sdp) {
    peerConnections[socketId].pc.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function () {
      // Only create answers in response to offers
      if (signal.sdp.type == 'offer') {
        peerConnections[socketId].pc.createAnswer().then(description => createdDescription(description, socketId)).catch(errorHandler);
      }
    }).catch(errorHandler);

  } else if (signal.ice) {
    peerConnections[socketId].pc.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
  }} catch{console.log('err ');console.log(signal)}
});

function setUpPeer(peerUuid, displayName, initCall = false) {
  peerConnections[peerUuid] = { 'displayName': displayName, 'pc': new RTCPeerConnection(pcConfig) };
  peerConnections[peerUuid].pc.ontrack = event => gotRemoteStream(event, peerUuid);//$.when(gotRemoteStream(event, peerUuid)).then(socket.emit("playerInfo", {socketId: peerUuid}));
  peerConnections[peerUuid].pc.onicecandidate = event => gotIceCandidate(event, peerUuid);
  peerConnections[peerUuid].pc.oniceconnectionstatechange = event => checkPeerDisconnect(event, peerUuid);
  peerConnections[peerUuid].pc.addStream(localStream);
  if (initCall) {
    peerConnections[peerUuid].pc.createOffer().then(description => createdDescription(description, peerUuid)).catch(errorHandler);
  }
}

function gotIceCandidate(event, peerUuid) {
  if (event.candidate != null) {
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'dest': peerUuid,
      'ice': event.candidate
    }
    sendMessage(msg)
    //serverConnection.send(JSON.stringify({ 'ice': event.candidate, 'uuid': localUuid, 'dest': peerUuid }));
  }
}

function createdDescription(description, peerUuid) {
  console.log(`got description, peer ${peerUuid}`);
  peerConnections[peerUuid].pc.setLocalDescription(description).then(function () {
    
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'dest': peerUuid,
      'sdp': peerConnections[peerUuid].pc.localDescription
    }
    sendMessage(msg)
    //serverConnection.send(JSON.stringify({ 'sdp': peerConnections[peerUuid].pc.localDescription, 'uuid': localUuid, 'dest': peerUuid }));
  }).catch(errorHandler);
}

function gotRemoteStream(event, peerUuid) {
  console.log(`got remote stream, peer ${peerUuid}`);
  
  if (!document.getElementById('remoteVideo_' + peerUuid)) {
    //assign stream to new HTML video element
    var vidElement = document.createElement('video');
    vidElement.setAttribute('autoplay', '');
    vidElement.setAttribute('muted', '');
    vidElement.setAttribute('id', 'remoteVideo_' + peerUuid);
    vidElement.setAttribute('width', 345);
    vidElement.setAttribute('height', 200);
    vidElement.srcObject = event.streams[0];

    document.getElementById('remotePlayerVideos').appendChild(vidElement);
    $(document).trigger('remotePlayerVideo', {"socketId":peerUuid});
  } else {
    document.getElementById('remoteVideo_' + peerUuid).srcObject = event.streams[0];
  }
  updateLayout();

}


// 유저 나감(유저가 나가면서 disconndet 통신을 보내고 나감)
socket.on("disconnectPlayer", function (socketId) {
  console.log("disconnectPlayer");
  var state = true;
  for (var i = 0; i < players.length; i++) {
    if (socketId == players[i].socketId) {
      $("#chat_content").append("<div>[안내] <strong>" + players[i].userName + "</strong>님 나감!</div>");
      $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
      removeAvatar(players[i], true);
      players.splice(i, 1);
      break;
    }
  }
});

// 유저 연결 확인(내 통신과 종료된 유저 확인)
function checkPeerDisconnect(event, peerUuid) {
  try{
    var state = peerConnections[peerUuid].pc.iceConnectionState;
    console.log(`connection with peer ${peerUuid} ${state}`);
    if (state === "failed" || state === "closed" || state === "disconnected") {
      console.log("checkPeerDisconnect");
      delete peerConnections[peerUuid];
      let node = document.getElementById('remoteVideo_' + peerUuid);
      if (node && node.parentNode) {
        node.parentNode.removeChild(node);
      }
      for (var j = 0; j < players.length; j++) {
        if (peerUuid == players[j].socketId) {
          $("#chat_content").append("<div>[안내] <strong>" + players[j].userName + "</strong>님 연결 종료</div>");
          $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
          removeAvatar(players[j],true);
          players.splice(j, 1);
          break;
        }
      }
      updateLayout();
    }
  }catch{
    
  }
}

function updateLayout() {
  // update CSS grid based on number of diplayed videos
  var rowHeight = '98vh';
  var colWidth = '98vw';

  var numVideos = Object.keys(peerConnections).length + 1; // add one to include local video

  if (numVideos > 1 && numVideos <= 4) { // 2x2 grid
    rowHeight = '48vh';
    colWidth = '48vw';
  } else if (numVideos > 4) { // 3x3 grid
    rowHeight = '32vh';
    colWidth = '32vw';
  }

  document.documentElement.style.setProperty(`--rowHeight`, rowHeight);
  document.documentElement.style.setProperty(`--colWidth`, colWidth);
}

function makeLabel(label) {
  var vidLabel = document.createElement('div');
  vidLabel.appendChild(document.createTextNode(label));
  vidLabel.setAttribute('class', 'videoLabel');
  return vidLabel;
}

function errorHandler(error) {
  console.log(error);
}