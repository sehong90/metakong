// socket.io 서버에 접속한다
var socket = io("wss://metaforming.com:3000");
//var socket = io('http://localhost:5010');

var players = [];

var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var pc;
var remoteStream;
var turnReady;

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  },
  {
    url: 'turn:numb.viagenie.ca',
    credential: 'muazkh',
    username: 'webrtc@live.com'
  }
  ]
};

var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

$(document).ready(function () {

  $('#chat_msg').keypress(function (evt) {
    if ((evt.keyCode || evt.which) == 13) {
      evt.preventDefault();
      send();
    }
  });
  // 서버로 자신의 정보를 전송한다.
  socket.emit("join", new BABYLON.Vector3(3, 0.07, 35));
});

// 총 접속자 수
socket.on("clientsCount", function (data) {
  $("#clients_count").html("현재 접속자 수 : " + data);
});

// 유저 닉네임 변경
socket.on("userNameUpdate", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      updatePlayerNameLabel(i, data.userName);
      break;
    }
  }
});

// 유저 입장
socket.on("join", function (playerList, newPlayer) {
  
  $('#name').val(newPlayer.userName);
  player_info.name = newPlayer.userName;
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + newPlayer.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  setNameLabel(newPlayer.socketId);
  console.log(playerList.length);
  if (playerList.length == 0){
    isInitiator = true;
  }

  // 기존 유저 생성
  for (var i = 0; i < playerList.length; i++) {
    //createPlayer(playerList[i].player);
  }
});

// 다른 유저 입장
socket.on("createPlayer", function (data) {
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + data.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  //createPlayer(data);
});

// 유저 나감
socket.on("disconnectPlayer", function (data) {
  var state = true;
  for (var i = 0; i < players.length; i++) {
    for (var j = 0; j < data.length; j++) {
      if (data[j].player.socketId == players[i].socketId) {
        state = false;
        break;
      }
    }
    if (!state) {
      $("#chat_content").append("<div>[안내] <strong>" + data[j].player.userName + "</strong>님 나감!</div>");
      $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
      removeAvatar(players[i]);
      players.splice(i, i);
    }
  }
});

// 유저 이동
socket.on("positionUpdate", function (positionData) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == positionData.socketId) {
      index = i;
      break;
    }
  }
  if (index > -1) {

    let player = players[index];
    player.player.position = positionData.position;
    player.player.rotation = new BABYLON.Vector3(0, positionData.rotation._y, 0);
    player.nameTag.position = new BABYLON.Vector3(positionData.position._x, positionData.position._y + 2, positionData.position._z);
  }
});

// 유저 액션 변경
socket.on("animationUpdate", function (data) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      index = i;
      break;
    }
  }
  if (index > -1) {
    let player = players[index].player;

    // 아바타 애니메이션 설정
    player.skeleton.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
    player.skeleton.animationPropertiesOverride.enableBlending = true;
    player.skeleton.animationPropertiesOverride.blendingSpeed = 1;
    player.skeleton.animationPropertiesOverride.loopMode = 1;

    var idleRange = skeleton.getAnimationRange("YBot_Idle");
    var walkRange = skeleton.getAnimationRange("YBot_Walk");
    var runRange = skeleton.getAnimationRange("YBot_Run");

    if (data.action == "walk") {
      scene.beginAnimation(player.skeleton, walkRange.from, walkRange.to, true);
    } else if (data.action == "run") {
      scene.beginAnimation(player.skeleton, runRange.from, runRange.to, true);
    } else {
      scene.beginAnimation(player.skeleton, idleRange.from, idleRange.to, true);
    }
  }
});

function sendMessage(message) {
  //console.log('Client sending message: ', message);
  socket.emit('message', message);
}

// 메시지
socket.on("message", function (message) {
  if (message.type === 'got user media') {
    for (var i = 0; i < players.length; i++) {
      if(players[i].socketId == message.socketId){
        players[i].streamId = message.streamId;
        players[i].pc = message.streamId;
      }
    }
    maybeStart();
  } else if (message.type === 'offer') {
    // 연결 요청이 왔음 
    // Offer 메시지를 setRemoteDescription 
    // Answer 메시지 생성 setLocalDescription and sendMessage
    console.log('offer');
    console.log(message)
    maybeStart();
    pc.setRemoteDescription(new RTCSessionDescription(message));
    doAnswer();
  } else if (message.type === 'answer') {
    // 요청 답변 왔음 setRemoteDescription
    console.log('answer');
    console.log(message)
    pc.setRemoteDescription(new RTCSessionDescription(message));
  } else if (message.type === 'candidate') {
    var candidate = new RTCIceCandidate({
      sdpMLineIndex: message.label,
      candidate: message.candidate
    });
    pc.addIceCandidate(candidate);
  } else if (message === 'bye') {
    handleRemoteHangup();
  }
});
socket.on('log', function (array) {
  console.log.apply(console, array);
});

// 메시지
socket.on("chat", function (data) {
  let today = new Date();
  let hours = ('0' + today.getHours()).slice(-2);
  let minutes = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2);
  $("#chat_content").append("<div><strong>" + data.from.name + " </strong>" + hours + ":" + minutes + ":" + seconds + "<p> " + data.msg + "</p></div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
});

function send() {
  var chat_msg = $('#chat_msg').val();
  if (chat_msg != '' && chat_msg != ' ') {
    // 서버로 메시지를 전송한다.
    socket.emit("chat", { msg: chat_msg });
    let today = new Date();
    let hours = ('0' + today.getHours()).slice(-2);
    let minutes = ('0' + today.getMinutes()).slice(-2);
    var seconds = ('0' + today.getSeconds()).slice(-2);
    $("#chat_content").append("<div><strong>" + $('#name').val() + " </strong>" + hours + ":" + minutes + ":" + seconds + "<p> " + chat_msg + "</p></div>");
    $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
    $('#chat_msg').val("");
  }
}

function createPeerConnection() {
  try {
    pc = new RTCPeerConnection(null);
    pc.onicecandidate = handleIceCandidate;
    pc.onaddstream = handleRemoteStreamAdded;
    pc.onremovestream = handleRemoteStreamRemoved;
    console.log('Created RTCPeerConnnection');
  } catch (e) {
    console.log('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event) {
  console.log('icecandidate event: ', event);
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    });
  } else {
    console.log('End of candidates.');
  }
}

function handleCreateOfferError(event) {
  console.log('createOffer() error: ', event);
}

function doCall() {
  // 스트리머 setLocal And SendMessage 
  console.log('Sending offer to peer');
  pc.createOffer(setLocalAndSendMessage, handleCreateOfferError);
}

function doAnswer() {
  console.log('Sending answer to peer.');
  pc.createAnswer().then(
    setLocalAndSendMessage,
    onCreateSessionDescriptionError
  );
}

function setLocalAndSendMessage(sessionDescription) {
  pc.setLocalDescription(sessionDescription);
  console.log('setLocalAndSendMessage sending message', sessionDescription);
  sendMessage(sessionDescription);
}

function onCreateSessionDescriptionError(error) {
  trace('Failed to create session description: ' + error.toString());
}

function requestTurn(turnURL) {
  var turnExists = false;
  for (var i in pcConfig.iceServers) {
    if (pcConfig.iceServers[i].urls.substr(0, 5) === 'turn:') {
      turnExists = true;
      turnReady = true;
      break;
    }
  }
  if (!turnExists) {
    console.log('Getting TURN server from ', turnURL);
    // No TURN server. Get one from computeengineondemand.appspot.com:
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var turnServer = JSON.parse(xhr.responseText);
        console.log('Got TURN server: ', turnServer);
        pcConfig.iceServers.push({
          'urls': 'turn:' + turnServer.username + '@' + turnServer.turn,
          'credential': turnServer.password
        });
        turnReady = true;
      }
    };
    xhr.open('GET', turnURL, true);
    xhr.send();
  }
}

function handleRemoteStreamAdded(event) {
  console.log('Remote stream added.');
  console.log(event.stream.id);

  var sid;

  for (var i = 0; i < players.length; i++) {
    if(players[i].streamId == event.stream.id){
      sid = players[i].socketId;
    }
  }
  const videoGrid = document.getElementById("videos");
  const video =  document.createElement("video");
  video.id="remoteVideo_"+sid;
  video.srcObject = event.stream;
  video.autoplay = true;
  videoGrid.append(video);
}

function handleRemoteStreamRemoved(event) {
  console.log('Remote stream removed. Event: ', event);
}

function hangup() {
  console.log('Hanging up.');
  stop();
  sendMessage('bye');
}

function handleRemoteHangup() {
  console.log('Session terminated.');
  stop();
  isInitiator = false;
}

function stop() {
  pc.close();
  pc = null;
}
