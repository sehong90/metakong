

var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');
var localAudio = document.querySelector('#localAudio');

const audioInputSelect = document.querySelector('select#micList');
const audioOutputSelect = document.querySelector('select#audioList');
const videoSelect = document.querySelector('select#videoList');
const selectors = [audioInputSelect, audioOutputSelect, videoSelect];

var webcamState = false;
function start() {
    if (window.stream) {
        window.stream.getTracks().forEach(track => {
            track.stop();
        });
    }
    const audioSource = audioInputSelect.value;
    const videoSource = videoSelect.value;
    const constraints = {
        audio: { deviceId: audioSource ? { exact: audioSource } : undefined },
        video: { deviceId: videoSource ? { exact: videoSource } : undefined }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(gotStream).then(gotDevices).catch(handleError);
}
function gotStream(stream) {
    console.log('Adding local stream.');
    window.stream = stream;
    localStream = stream;
    screanStream = stream;
    localVideo.srcObject = stream;

    msg = {
        'socketId': socket.id,
        'streamId': localStream.id,
        'displayName': '콩콩쓰', 
        'dest': 'all'
    }
    sendMessage(msg);

    webcamState = true;
    return navigator.mediaDevices.enumerateDevices();
}

function maybeStart() {
    console.log('>>>>>>> maybeStart() ', localStream);
    if (typeof localStream !== 'undefined') {
        console.log('>>>>>> creating peer connection');
        // RTCPeerConnection
        createPeerConnection();
        pc.addStream(localStream);
        if(isInitiator){
            // 스트리머는 Offer 메시지 생성
            // Offer 메시지 setLocalDescription에 등록
            // setLocalAndSendMessage() 시그널링 서버에게 전달
            doCall();
        }
    }
} 

function gotDevices(deviceInfos) {
    // Handles being called several times to update labels. Preserve values.
    const values = selectors.map(select => select.value);
    selectors.forEach(select => {
        while (select.firstChild) {
            select.removeChild(select.firstChild);
        }
    });
    
    for (let i = 0; i !== deviceInfos.length; ++i) {
        const deviceInfo = deviceInfos[i];
        const option = document.createElement('option');
        option.value = deviceInfo.deviceId;
        if (deviceInfo.kind === 'audioinput') {
            option.text = deviceInfo.label || `microphone ${audioInputSelect.length + 1}`;
            audioInputSelect.appendChild(option);
        } else if (deviceInfo.kind === 'audiooutput') {
            option.text = deviceInfo.label || `speaker ${audioOutputSelect.length + 1}`;
            audioOutputSelect.appendChild(option);
        } else if (deviceInfo.kind === 'videoinput') {
            option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
            videoSelect.appendChild(option);
        } else {
            console.log('Some other kind of source/device: ', deviceInfo);
        }
    }
    selectors.forEach((select, selectorIndex) => {
        if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex])) {
            select.value = values[selectorIndex];
        }
    });
}

function attachSinkId(element, sinkId) {
    if (typeof element.sinkId !== 'undefined') {
        element.setSinkId(sinkId)
            .then(() => {
                console.log(`Success, audio output device attached: ${sinkId}`);
            })
            .catch(error => {
                let errorMessage = error;
                if (error.name === 'SecurityError') {
                    errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
                }
                console.error(errorMessage);
                // Jump back to first output device in the list as it's the default.
                audioOutputSelect.selectedIndex = 0;
            });
    } else {
        console.warn('Browser does not support output device selection.');
    }
}

function changeAudioDestination() {
    const audioDestination = audioOutputSelect.value;
    attachSinkId(localVideo, audioDestination);
}

function handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

audioInputSelect.onchange = start;
audioOutputSelect.onchange = changeAudioDestination;

videoSelect.onchange = start;

start();

function shareScreen(){
  try {
    navigator.mediaDevices.getDisplayMedia({
		audio: true,
		video: true
	}).then(function(stream){

        stream.getTracks()[0].addEventListener('ended', () => {
            console.log("ended");
            stopStreaming();
        })
		localStream = stream;
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰', 
            'dest': 'all'
        }
        sendMessage(msg);
        // 웹캠 스트리밍 공유
        addVideoStream("screen",localStream, player_info.name);
        $('.button-stop').css('display','block');
        socket.emit("streamScreen", { socketId: socket.id, streamer : player_info.name });

        // 설정창 닫기
        $('.modal').hide(); 
        document.getElementById('setting').innerText="설정";
    }).catch(function(e){
		//error;
	});
    
   
  } catch (err) {
    console.error("Error: " + err);
  }
}

function toggleSetting() {
    var display = document.getElementsByClassName("modal")[0].style.display
    if (display == "none") {
        $('.name-box').show();
        //$('.button-video').hide();
        $('.url-box').hide();
        $('.modal').show();
        $('.video-select').show();
        document.getElementById('setting').innerText = "닫기";
    } else {
        $('.modal').hide();
        document.getElementById('setting').innerText = "설정";

    }
}
function toggleChatting() {
    var button = document.querySelector('.button-chatting');
    var chatting = document.querySelector('.li-chatting');

    if (button.style.display === 'none') {
        button.style.display = 'block';
        chatting.style.display = 'none';
    } else {
        button.style.display = 'none';
        chatting.style.display = 'block';
    }

}

let mic_switch = true;
let video_switch = true;

function toggleVideo() {
    if (localStream != null && localStream.getVideoTracks().length > 0) {
        video_switch = !video_switch;
        localStream.getVideoTracks()[0].enabled = video_switch;
    }
}

function toggleMic() {
    if (localStream != null && localStream.getAudioTracks().length > 0) {
        var toggle = document.getElementById('toggle-mic');
        if (toggle.getAttribute('data') == 'off'){
           $('.toggle-mic-on').css('display','block');
           $('.toggle-mic-off').css('display','none');
            toggle.setAttribute('data', 'on');
        } else {
           $('.toggle-mic-on').css('display','none');
           $('.toggle-mic-off').css('display','block');
            toggle.setAttribute('data', 'off');
        }
        mic_switch = !mic_switch;
        localStream.getAudioTracks()[0].enabled = mic_switch; 
    }
}
function toggleAvatar(){
    msg = {
        socketId: socket.id,
        state: player_info.avatarState
    }
    socket.emit("userAvatarUpdate", msg);
    userAvatarUpdate(null, msg);
    
    var toggle = document.getElementById('toggle-video');
    if (toggle.getAttribute('data') == 'off'){
        $('.toggle-video-on').css('display','block');
       $('.toggle-video-off').css('display','none');
        toggle.setAttribute('data', 'on');
    } else {
       $('.toggle-video-on').css('display','none');
       $('.toggle-video-off').css('display','block');
        toggle.setAttribute('data', 'off');
    }
}

function fullScreen(){
    var stream_state =$('#stream_state').val();
    console.log(stream_state);
    if (stream_state =='url'){
        let node = document.getElementById("iframe-video").src;
        var newWindow = window.open("about:blank");
        newWindow.location.href = node;
    } else if (stream_state =='webCam' ||  stream_state =='screen'){
        $("#popup").fadeIn();
    }
}

$(document).ready(function () {

$("#popup .exit").click(function() {
    $("#popup").fadeOut();
});
});

    window.onbeforeunload = function () {
        sendMessage('bye');
    };